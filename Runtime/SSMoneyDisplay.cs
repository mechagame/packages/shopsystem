﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace ShopSystem
{
    public class SSMoneyDisplay : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI moneyText;
        [SerializeField] private SSMoneyInventory moneyInventory;

        private void Awake()
        {
            moneyInventory.onMoneyChanged.AddListener(DisplayMoney);
        }

        private void OnDestroy()
        {
            moneyInventory.onMoneyChanged.RemoveListener(DisplayMoney);
        }

        public void DisplayMoney(int money)
        {
            moneyText.text = money.ToString();
        }
    }
}
