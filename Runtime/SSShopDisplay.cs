﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace ShopSystem
{
    public class SSShopDisplay : MonoBehaviour
    {
        [SerializeField] private Transform displayItemPosition;
        [SerializeField] private TextMeshPro priceDisplayText;
        [SerializeField] private TextMeshPro nameDisplayText;
        [SerializeField] private SSShop shop;
        private GameObject currentDisplayItem;

        private void Awake()
        {
            shop.onRestock.AddListener(ResetDisplay);
        }

        private void OnDisable()
        {
            if (currentDisplayItem == null) { return; }
            currentDisplayItem.SetActive(false);
        }

        private void OnDestroy()
        {
            shop.onRestock.RemoveListener(ResetDisplay);
        }

        public void ResetDisplay(SSItemData currentItem)
        {
            if (currentDisplayItem != null) { currentDisplayItem.SetActive(false); }
            if (currentItem == null)
            {
                nameDisplayText.text = "";
                priceDisplayText.text = "";
                return;
            }
            currentDisplayItem = SSPool.GetInstance.GetItemFromPool(
                currentItem.GetDisplayItem,
                displayItemPosition.position,
                Quaternion.identity
                );
            currentDisplayItem.transform.SetParent(displayItemPosition);
            nameDisplayText.text = currentItem.name;
            priceDisplayText.text = currentItem.GetCost.ToString();
        }
    }
}
