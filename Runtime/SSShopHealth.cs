﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class SSShopHealth : MonoBehaviour
{
    [SerializeField] private int maxHealth;
    private int currentHealth;

    public UnityEvent onDestroyed;

    private void OnEnable()
    {
        currentHealth = maxHealth;
    }

    public void TakeDamage(int damage)
    {
        currentHealth -= damage;
        currentHealth = Mathf.Clamp(currentHealth, 0, maxHealth);
        if(currentHealth != 0) { return; }
        onDestroyed?.Invoke();
    }
}
