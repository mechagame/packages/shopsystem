﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShopSystem
{
    public class SSCoinCollector : MonoBehaviour
    {
        [SerializeField] private SSMoneyInventory inventory;
        private void OnTriggerEnter(Collider other)
        {
            if (other.TryGetComponent(out SSCoin coin))
            {
                inventory.ModifyMoney(coin.GetValue);
                coin.Collect();
            }
        }
    }
}
