﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ShopSystem
{
    public class SSMoneyInventory : MonoBehaviour
    {
        [SerializeField] private int maxMoney;
        [SerializeField] private int startMoney;

        private int money;
        public int GetMoney => money;

        [Serializable] public class OnMoneyChangedEvent : UnityEvent<int> { }
        public OnMoneyChangedEvent onMoneyChanged;

        private void Start()
        {
            ModifyMoney(startMoney);
        }

        public void ModifyMoney(int amount)
        {
            money += amount;
            money = Mathf.Clamp(money, 0, maxMoney);
            onMoneyChanged?.Invoke(money);
        }
    }
}
