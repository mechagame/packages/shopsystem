﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ShopSystem
{
    public class SSShopManager : SSSingleton<SSShopManager>
    {
        public UnityEvent onBuyFailed;
        public UnityEvent onBuySucceeded;

        public void Succeed() { onBuySucceeded?.Invoke(); }
        public void Fail() { onBuyFailed?.Invoke(); }
    }
}
