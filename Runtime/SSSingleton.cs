﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShopSystem
{
    public abstract class SSSingleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        private static T instance;
        public static T GetInstance => instance;

        protected virtual void Awake() => SetAsSingleton();

        private void SetAsSingleton()
        {
            if (instance)
            {
                Destroy(this);
            }
            else
            {
                instance = this as T;
            }
        }
    }
}
