﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShopSystem
{
    public class SSItemDrop : MonoBehaviour
    {
        [SerializeField] private Transform dropPosition;

        public void DropItem(GameObject item)
        {
            GameObject newItem = SSPool.GetInstance.GetItemFromPool(item, dropPosition.position, Quaternion.identity);
        }
    }
}
