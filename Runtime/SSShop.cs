﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.Events;

namespace ShopSystem
{
    public class SSShop : MonoBehaviour
    {
        [SerializeField] private bool restockOnStart;
        [SerializeField] private int autoRestockAmount = 0;
        [SerializeField] private SSItemDrop itemDrop;

        [SerializeField] private List<SSItemData> items;
        private SSItemData currentItem;

        [Serializable] public class OnRestockEvent : UnityEvent<SSItemData> { }
        public OnRestockEvent onRestock;

        private void Start()
        {
            if (items.Count <= 0) { return; }
            if (restockOnStart) { Restock(); }
        }

        public void Restock()
        {
            List<SSItemData> _newItems = new List<SSItemData>();
            for (int i = 0; i < items.Count; i++)
            {
                for (int j = 0; j < items[i].GetProbability; j++)
                {
                    _newItems.Add(items[i]);
                }
            }

            int random = UnityEngine.Random.Range(0, _newItems.Count);
            currentItem = _newItems[random];
            onRestock?.Invoke(currentItem);
            autoRestockAmount--;
        }

        public bool TryBuyItem(int currentMoney, out int itemCost)
        {
            itemCost = 0;
            if (currentItem == null) { return false; }
            itemCost = currentItem.GetCost;
            if (currentMoney < currentItem.GetCost)
            {
                SSShopManager.GetInstance.Fail();
                return false;
            }
            itemDrop.DropItem(currentItem.GetItem);
            SSShopManager.GetInstance.Succeed();
            if (autoRestockAmount <= 0) { onRestock?.Invoke(null); currentItem = null; return true; }
            Restock();
            return true;
        }
    }
}
