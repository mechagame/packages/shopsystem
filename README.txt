Ce package contient un système de boutique.

---Ajouter le pool---
-Ajouter à un gameobject "SS Pool", qui servira à pool les objets créés par le système

---Créer un objet à vendre---
-Dans les assets, faire clic droit create/shopsystem/itemdata
-Y indiquer le préfab qui servira de display, le préfab qui sera loot, le cout de l'objet et la probabilité qu'il a d'apparaître.

---Créer une boutique---
-Ajouter les scripts "Shop", "Shop Display" et "Item Drop" à un gameobject
-Ajouter un rigidbody et un collider à cet objet
-Donner à cet objet un layer qui servira à l'interaction avec le joueur
-Dans "Shop", indiquer si la boutique se restocke automatiquement au start, et combien de fois elle se restocke automatiquement.
-Indiquer tout les objets que peut vendre cette boutique (représentés par les itemDatas)
-Dans l'événement "OnRestock" du shop, appeler la fonction "ResetDisplay" du "ShopDisplay"
-Dans "ShopDisplay", indiquer le textmeshpro qui montrera le prix de l'objet, ainsi que la position 
du préfab qui sera display.
-Dans "ItemDrop", indiquer l'endroit où va apparaître l'objet une fois acheté.

---Restocker la boutique manuellement---
-Appeler la fonction "Restock" d'une boutique spécifique

---Gérer l'argent du personnage---
-Rajouter au joueur un "Money Inventory" 
-Appeler la fonction "ModifyMoney" pour ajouter ou retirer de l'argent.

---Acheter des objets---
-Ajouter un script "ShopInteract" au joueur.
-Y indiquer le layer du shop et la distance à laquelle il doit pouvoir interagir avec les shops
-Appeler la fonction "InteractWithShop"
-Le joueur va chercher le shop le plus proche dans son rayon d'interaction.
-Si l'argent du joueur est supérieur ou égal au prix de l'objet actuel du shop, le shop va laisser tomber
l'objet et le joueur se verra retirer son prix.

