﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShopSystem
{
    [CreateAssetMenu(menuName = "ShopSystem/ItemData")]
    public class SSItemData : ScriptableObject
    {
        [SerializeField] private GameObject item;
        public GameObject GetItem => item;
        [SerializeField] private GameObject displayItem;
        public GameObject GetDisplayItem => displayItem;
        [SerializeField] private int cost;
        public int GetCost => cost;
        [SerializeField] private int probability;
        public int GetProbability => probability;
    }
}
