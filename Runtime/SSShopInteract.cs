﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ShopSystem
{
    public class SSShopInteract : MonoBehaviour
    {
        [SerializeField] private LayerMask interactMask;
        [SerializeField] private float interactDistance;
        [SerializeField] private SSMoneyInventory inventory;

        public void InteractWithShop()
        {

            Collider[] interactableColliders = Physics.OverlapSphere(transform.position, interactDistance, interactMask);
            if (interactableColliders.Length <= 0) { return; }

            SSShop closestShop = null;

            if (interactableColliders.Length == 1)
            {
                closestShop = interactableColliders[0].GetComponent<SSShop>();
            }
            else
            {
                float bestDistance = Mathf.Infinity;
                Collider bestCollider = null;
                for (int i = 0; i < interactableColliders.Length; i++)
                {
                    float distance = Vector3.Distance(transform.position, interactableColliders[i].gameObject.transform.position);
                    if (distance < bestDistance)
                    {
                        bestDistance = distance;
                        bestCollider = interactableColliders[i];
                    }
                }
                closestShop = bestCollider.gameObject.GetComponent<SSShop>();
            }

            if (closestShop.TryBuyItem(inventory.GetMoney, out int itemCost))
            {
                inventory.ModifyMoney(-itemCost);
            }
        }
    }
}
