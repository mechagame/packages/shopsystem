﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace ShopSystem
{
    public class SSCoin : MonoBehaviour
    {
        [SerializeField] private int value;
        public int GetValue => value;

        public UnityEvent onCollected;

        public void Collect()
        {
            onCollected?.Invoke();
            gameObject.SetActive(false);
        }
    }
}
